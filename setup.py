#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup
import wallet

with open('README.md') as readme_file:
	readme = readme_file.read()

requirements = [
	'pyqt5',
	'ofxparse',
	'pyqtgraph'
]

dev_requirements = [
	'pip',
	'virtualenv'
]

setup(
	name=wallet.__name__,
	install_requires=requirements,
	extras_require={'dev': dev_requirements}
)
