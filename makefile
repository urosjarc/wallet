include config/makefile

#============================
### START DEVELOPING ########
#============================

setup: ## execute setup script
	sudo add-apt-repository -y ppa:fkrull/deadsnakes || echo 'Fail deadsnakes'
	sudo apt-get update
	sudo apt-get install -y build-essential python3.6-dev python3-pip python3.6-tk zip unzip

	$(MAKE) install
	$(MAKE) decript

install: clean-venv ## start virtual environment and install dev. requirements
	virtualenv -p python3.6 --distribute $(VIRTUAL_ENV) || virtualenv --distribute $(VIRTUAL_ENV)
	$(MAKE) install-dep

install-dep: clean-py ## install development libs
	pip install -e .[dev]
	pip install -e .

#============================
### RUNNING #################
#============================

encript: ## encript data dir
	zip --encrypt -r data.zip data

decript: ## descript data zip
	unzip data.zip

run: ## run package script
	@python $(PACKAGE)

#============================
### TESTING #################
#============================

dep: ## test dependencies
	pip list --outdated --format=columns
	@if [ -z "$$(pip list --outdated --format=columns)" ]; then exit 0; else exit 1; fi

#============================
### BUILD ###################
#============================

dist: clean-py ## builds source and wheel package
	python setup.py sdist
	python setup.py bdist_wheel
	ls -l dist

#============================
### CLEANING ################
#============================

clean: clean-venv clean-py # clean all setup files/folders

clean-venv: clean-py #clean virtual environment folder
	rm -fr $(VIRTUAL_ENV)

clean-py: clean-build clean-pyc ## remove all Python files/folders

clean-build: ## remove build artifacts
	rm -fr build
	rm -fr dist
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +
	find . -name '*.tar.gz' -exec rm -f {} +

clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +
