import sys
import re
from PyQt5 import QtCore, QtGui
from PyQt5.uic import loadUi
from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox, QTreeWidgetItem
from wallet import path, data, utils
import datetime
import pyqtgraph as pg

app = QApplication(sys.argv)


class TimeAxisItem(pg.AxisItem):
	def __init__(self, *args, **kwargs):
		super(TimeAxisItem, self).__init__(*args, **kwargs)

	def tickStrings(self, values, scale, spacing):
		return [datetime.datetime.fromtimestamp(value).strftime("%Y %m %d") for value in values]


class MainWindow(QMainWindow):
	def __init__(self, *args, **kwargs):
		super(MainWindow, self).__init__(parent=None, *args, **kwargs)
		loadUi(path.fileDirJoin(__file__, 'main.ui'), self)

		self.report = data.Report(
			data.load(path.app('data'))
		)

		# Main plot item
		self.plotItem = self.graphicsLayoutWidget.addPlot(axisItems={'bottom': TimeAxisItem(orientation='bottom')})
		self.meanPlot = {'x': None, 'y': None}
		self.goalPlot = {'x': None, 'y': None}

		# cross hair
		self.vLine = pg.InfiniteLine(angle=90, movable=False)
		self.plotItem.addItem(self.vLine, ignoreBounds=True)
		self.hLine = pg.InfiniteLine(angle=0, movable=False)
		self.plotItem.addItem(self.hLine, ignoreBounds=True)

		# value item
		self.valueItem = pg.TextItem()
		self.valueItem.setPos(
			utils.dateToTimestamp(self.report.data.ofx.balance.dates)[0],
			self.report.data.ofx.balance.values[0]
		)
		self.plotItem.addItem(self.valueItem)

		# target item
		self.targetItem = pg.TextItem()
		self.targetItem.setPos(
			utils.dateToTimestamp(self.report.data.ofx.balance.dates)[0],
			self.report.data.ofx.balance.values[0]
		)
		self.plotItem.addItem(self.targetItem)

		# diff item
		self.diffItem = pg.TextItem()
		self.diffItem.setPos(
			utils.dateToTimestamp(self.report.data.ofx.balance.dates)[0],
			self.report.data.ofx.balance.values[0]
		)
		self.plotItem.addItem(self.diffItem)

		# date item
		self.dateItem = pg.TextItem()
		self.dateItem.setPos(
			utils.dateToTimestamp(self.report.data.ofx.balance.dates)[0],
			self.report.data.ofx.balance.values[0]
		)
		self.plotItem.addItem(self.dateItem)

		self._connect()
		self.update()

	def _connect(self):
		self.plotItem.scene().sigMouseMoved.connect(self.mouseOnGraphMoved)
		self.expensesTW.itemClicked.connect(self.expensesItemClicked)
		self.reportA.triggered.connect(self.reportDialog)

	def expensesItemClicked(self, item: QTreeWidgetItem):
		targetDateStr = item.text(0)
		self.mouseOnGraphMoved(date=datetime.datetime.strptime(targetDateStr, '%y-%m-%d'))

	def mouseOnGraphMoved(self, evt=None, date=None):

		if date is not None or self.plotItem.sceneBoundingRect().contains(evt):

			if date is None:
				mousePoint = self.plotItem.vb.mapSceneToView(evt)
				targetDate = mousePoint.x()
			else:
				targetDate = date.timestamp()

			targetDateStr = datetime.datetime.fromtimestamp(targetDate).strftime('%y-%m-%d')

			values = self.report.data.ofx.balance.values
			dates = utils.dateToTimestamp(self.report.data.ofx.balance.dates)

			for i in range(1, len(dates)):

				if dates[i - 1] <= targetDate < dates[i]:

					# Scrool to item
					dateMatch = self.expensesTW.findItems(targetDateStr, QtCore.Qt.MatchContains)
					if len(dateMatch) > 0:
						self.expensesTW.selectionModel().clearSelection()
						self.expensesTW.scrollToItem(dateMatch[0])
						dateMatch[0].setSelected(True)

					# value item
					k = (values[i - 1] - values[i]) / (dates[i - 1] - dates[i])
					targetValue = int(k * (targetDate - dates[i - 1]) + values[i - 1])
					self.valueItem.setHtml("<span style='color: yellow'>{}€</span>".format(targetValue))
					self.valueItem.setAnchor((1, 0) if k < 0 else (1, 1))
					self.valueItem.setPos(targetDate, targetValue)

					# date item
					self.dateItem.setHtml("<span style='color: yellow'>{}</span>".format(
						datetime.datetime.fromtimestamp(targetDate).strftime(self.report.data.config.dateFormat)
					))
					self.dateItem.setPos(targetDate, 0)

					# crosshair
					self.vLine.setPos(targetDate)
					self.hLine.setPos(targetValue)

			if self.meanPlot['x'][0] < targetDate < self.meanPlot['x'][1]:

				# Mean item
				k = (self.meanPlot['y'][0] - self.meanPlot['y'][1]) / (self.meanPlot['x'][0] - self.meanPlot['x'][1])
				meanValue = k * (targetDate - self.meanPlot['x'][0]) + values[-1]
				self.valueItem.setHtml("<span style='color: yellow'>{}€</span>".format(int(meanValue)))
				self.valueItem.setPos(targetDate, meanValue)
				self.valueItem.setAnchor((1, 0) if k < 0 else (1, 1))

				# Target item
				meanK = (self.targetPlot['y'][0] - self.targetPlot['y'][1]) / (
					self.targetPlot['x'][0] - self.targetPlot['x'][1])
				goalValue = meanK * (targetDate - self.targetPlot['x'][0]) + values[-1]
				self.targetItem.setHtml("<span style='color: green'>{}€</span>".format(int(goalValue)))
				self.targetItem.setPos(targetDate, goalValue)
				self.targetItem.setAnchor((1, 0) if meanK < 0 else (1, 1))

				# Diff item
				diffValue = (meanValue - goalValue)
				self.diffItem.setHtml(
					"<span style='color: {}'>{}€</span>".format('green' if diffValue >= 0 else 'red', int(diffValue)))
				self.diffItem.setPos(targetDate, (meanValue + goalValue) / 2)
				self.diffItem.setAnchor((1, 0) if meanK < 0 else (1, 1))

				# Date item
				self.dateItem.setHtml("<span style='color: yellow'>{}</span>".format(targetDateStr))
				self.dateItem.setPos(targetDate, 0)

				# Crosshair
				self.vLine.setPos(targetDate)
				self.hLine.setPos(meanValue)

			elif self.meanPlot['x'][0] > targetDate:
				self.targetItem.setHtml('')
				self.diffItem.setHtml('')

	def update(self):
		self.updateGraph()
		self.updateExpenses()

	def updateGraph(self):
		self.report = data.Report(
			data.load(path.app('data'))
		)

		balance = self.report.data.ofx.balance
		values = balance.values
		dates = balance.dates

		# Normal graph
		self.plotItem.plot(x=utils.dateToTimestamp(dates), y=values)

		# Mean graph
		self.meanPlot = {
			'x': utils.dateToTimestamp([dates[-1], self.report.goalEndDate]),
			'y': [values[-1], self.report.balanceAtGoal]
		}
		self.plotItem.plot(x=self.meanPlot['x'], y=self.meanPlot['y'], pen=pg.mkPen('y', style=QtCore.Qt.DotLine))

		# Target graph
		self.targetPlot = {
			'x': self.meanPlot['x'],
			'y': [values[-1], self.report.targetedBalanceAtGoal]
		}
		self.plotItem.plot(x=self.targetPlot['x'], y=self.targetPlot['y'], pen=pg.mkPen('g', style=QtCore.Qt.DotLine))

		# Monthly graph
		balance = self.report.data.ofx.monthBalance
		values = balance.values
		dates = balance.dates
		self.plotItem.plot(x=utils.dateToTimestamp(dates), y=values, pen=pg.mkPen(style=QtCore.Qt.DotLine))

	def reportDialog(self):
		msg = QMessageBox()
		msg.setText(str(self.report))
		msg.setDetailedText('Goals to acchive:\n - ' + '\n - '.join(self.report.data.config.goalComments))
		msg.exec()

	def updateExpenses(self):

		self.expensesTW.clear()
		obj = self.expensesTW
		value = self.report.data.ofx.getJSON()

		toStr = lambda vals: [str(val) for val in vals]

		def color(tran):
			intensity = 255 - 120 * utils.sigmoid((abs(float(tran)) - 100) / 50)
			col = QtGui.QColor()
			if tran < 0:
				col.setHsl(360, 50 * 255 / 100.0, intensity)
			else:
				col.setHsl(120, 50 * 255 / 100.0, intensity)

			col.setAlpha(255 * utils.sigmoid(abs(tran)))
			return col

		for i, month in enumerate(value):
			if len(value) - 1 > i:
				endBalance = value[i + 1]['balance']
			else:
				endBalance = 0

			income = month['balance'] + endBalance
			monthItem = QTreeWidgetItem(obj, toStr(
				[month['start_date'].strftime(self.report.data.config.dateFormat), month['balance'],
				 income]))
			monthItem.setBackground(2, color(income))

			for j, tran in enumerate(month['trans']):
				tranItem = QTreeWidgetItem(monthItem, toStr([
					tran['date'].strftime('%y-%m-%d'),
					tran['balance'],
					tran['amount'],
					tran['id'],
					tran['type'],
					tran['memo'],
					tran['payee']
				]))

				income = tran['amount']
				tranItem.setBackground(2, color(income))


def main():
	mainWindow = MainWindow()
	mainWindow.show()
	sys.exit(app.exec_())
