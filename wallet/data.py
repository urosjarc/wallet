from typing import List
import pathlib
from wallet import path, utils
import json
import datetime


class AccountBalance(object):
	def __init__(self, dates: List[datetime.datetime], balanceVals: List[float]):
		self.dates = dates
		self.values = balanceVals


class Data(object):
	def __init__(self, config: dict, ofx: list):
		self.config: Config = Config(config)
		self.ofx: OFX = OFX(self.config.startBalance, ofx)


class OFX(object):
	def __init__(self, startBalance, accountStatements: list):

		self.accountStatements = accountStatements
		self.startBalance = startBalance

		self.balance: AccountBalance = None
		self.monthBalance: AccountBalance = None

		self.init()

	def getJSON(self):
		report = [
			{
				'start_date': self.accountStatements[0].start_date,
				'balance': None,
				'trans': []
			}
		]

		balance = self.startBalance

		for i, state in enumerate(self.accountStatements):

			report[i]['balance'] = balance

			for tran in state.transactions:
				balance += tran.amount
				report[i]['trans'].append(tran.__dict__)
				report[i]['trans'][-1]['balance'] = balance

			if len(self.accountStatements) - 1 != i:
				report.append({
					'start_date': state.end_date,
					'balance': state.balance,
					'trans': []
				})

		return report

	def init(self):

		self.accountStatements.sort(key=lambda state: state.start_date)

		x = [self.accountStatements[0].start_date]
		y = [self.startBalance]

		monthX, monthY = [x[0]], [y[0]]

		for i, state in enumerate(self.accountStatements):

			state.transactions.sort(key=lambda tran: tran.date)

			for tran in state.transactions:
				x.append(tran.date)
				y.append(y[-1] + tran.amount)

			y.append(state.balance)
			x.append(state.end_date)

			monthX.append(state.end_date)
			monthY.append(state.balance)

		self.balance = AccountBalance(x, utils.toFloat(y))
		self.monthBalance = AccountBalance(monthX, utils.toFloat(monthY))


class Config(object):
	def __init__(self, config: dict):
		self.dateFormat = config['format']['date']
		self.startBalance = config['startBalance']['amount']
		self.goalBalance = config['goalBalance']['amount']
		self.goalEndDate = datetime.datetime(**config['goalBalance']['end'])
		self.goalComments = config['goalBalance']['comments']


class Report(object):
	def __init__(self, data: Data):
		self.data = data

		self.goalEndDate = data.config.goalEndDate
		self.currentBalance = data.ofx.balance.values[-1]
		self.targetedBalanceAtGoal = data.config.goalBalance

		self.meanExpense = {
			'day': None,
			'month': None,
			'year': None
		}

		self.days = (data.ofx.balance.dates[-1] - data.ofx.balance.dates[0]).days
		self.months = utils.diffMonth(data.ofx.balance.dates[-1], data.ofx.balance.dates[0])

		self.daysTillGoal = (data.config.goalEndDate - data.ofx.balance.dates[-1]).days
		self.monthsTillGoal = utils.diffMonth(data.config.goalEndDate, data.ofx.balance.dates[-1])

		self.balanceAtGoal = None

		self.goalIncome = {
			'day': None,
			'month': None,
			'year': None
		}

		self.init()

	def __str__(self):
		report = ''
		report +='Income\tMean\tTarget:'
		for key, value in self.meanExpense.items():
			report += '\n- {}:\t- {}€\t- {}€'.format(key, value, self.goalIncome[key])

		report += '\n\nEnd Balance in {} months:\n {}€ + {}€ = {}€'.format(
			self.monthsTillGoal,
			self.currentBalance,
			self.balanceAtGoal + self.currentBalance,
			self.balanceAtGoal
		)

		diffTargetRealBalanceAtGoal = self.balanceAtGoal - self.targetedBalanceAtGoal
		report += '\n\nEnd balance difference:\n {}€ - {}€ = {}€\n\n{}'.format(
			self.balanceAtGoal,
			self.targetedBalanceAtGoal,
			diffTargetRealBalanceAtGoal,
			'Target not reached' if diffTargetRealBalanceAtGoal < 0 else 'Target in reached'
		)

		return report

	def init(self):
		balance = self.data.ofx.balance

		# Average exp
		balanceDiff = (balance.values[-1] - balance.values[0])
		meanExpDay = balanceDiff / self.days
		self.meanExpense['day'] = round(meanExpDay, 2)
		self.meanExpense['month'] = round(balanceDiff / self.months, 0)
		self.meanExpense['year'] = round(meanExpDay * 365, 0)

		# Goal analise
		self.balanceAtGoal = round(self.currentBalance + self.daysTillGoal * meanExpDay)

		# Goal income
		targetedIncome = (self.targetedBalanceAtGoal - self.currentBalance)
		self.goalIncome['day'] = round(targetedIncome / self.daysTillGoal, 2)
		self.goalIncome['month'] = round(targetedIncome / self.monthsTillGoal, 0)
		self.goalIncome['year'] = round(self.goalIncome['day'] * 365, 0)

def load(dir) -> Data:
	states = []
	for ofxPath in sorted(pathlib.Path(dir).glob('**/*.ofx')):
		states.append(utils.loadOFX(ofxPath.absolute()))

	with open(path.app(dir, 'config.json')) as file:
		config = json.load(file)

	return Data(config, states)


if __name__ == '__main__':
	data = load(path.app('data'))
	report = Report(data)
