from ofxparse import OfxParser
import io
import math

def diffMonth(d1, d2):
	return (d1.year - d2.year) * 12 + d1.month - d2.month

def toFloat(vals: list):
	return [float(val) for val in vals]

def dateToTimestamp(dates):
	return [date.timestamp() for date in dates]

def loadOFX(filePath: str) -> list:

	dic = {
		'š': 's',
		'đ': 'dz',
		'ž': 'z',
		'č': 'c',
		'ć': 'c',
		'Š': 'S',
		'Đ': 'DZ',
		'Ž': 'Z',
		'Č': 'C',
		'Ć': 'C'
	}

	with open(filePath, 'r') as file:
		data = file.read()
		for key, value in dic.items():
			data = data.replace(key, value)
		ofx = OfxParser.parse(io.BytesIO(data.encode()))
		return ofx.account.statement

def sigmoid(x):
	return 1 / (1 + math.exp(-x))
