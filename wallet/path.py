import os
import sys
from typing import Union

this = sys.modules[__name__]
this.appBase = os.path.join(__file__, '../..')


def join(*paths: Union[tuple, str]) -> str:
	return os.path.normpath(
		os.path.join(*paths)
	)


def fileDirJoin(_file_: str, *paths: Union[tuple, str]) -> str:
	return os.path.normpath(
		os.path.join(os.path.dirname(os.path.realpath(_file_)), *paths)
	)


def app(*paths: Union[tuple, str]) -> str:
	return os.path.normpath(
		os.path.join(this.appBase, *paths)
	)
